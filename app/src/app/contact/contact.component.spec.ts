import { Contact } from '../model/contact';
import { ContactComponent } from './contact.component';

describe('ContactComponent', () => {

  // Variables
  let contactComponent: ContactComponent;
  // Before Each Async
  beforeEach(async () => {
  });
  // Before Each
  beforeEach(() => {
    contactComponent = new ContactComponent();
    console.log('Instanciation composant ContactComponent', contactComponent);
  });
  it('should create', () => {
    expect(contactComponent).not.toBeNull();
  });
  it('should be no contacts if there is no data', () => {
    expect(contactComponent.contacts.length).toEqual(0);
  });
  it('should be contacts if there is data', () => {
    const contact: Contact = {
      name: 'Test'
    };
    contactComponent.contacts.push(contact);
    expect(contactComponent.contacts.length).toBeGreaterThan(0);
  });
});
