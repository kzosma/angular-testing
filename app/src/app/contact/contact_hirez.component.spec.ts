import { ContactComponent } from './contact.component';
import { Contact } from '../model/contact';
describe('Test Component Contact with GWT', () => {

  let contactComponent: ContactComponent;
  Given(() => {
    console.log('Hirez Create new Contact Component');
    contactComponent = new ContactComponent();
  });

  describe('should be no contacts if there is no data', () => {
    When(() => {});
     Then(() => {
      expect(contactComponent.contacts.length).toEqual(0);
    });
  });
  describe('should be contacts if there is data', () => {
    When(() => {
      const contact: Contact = {name: 'Test'};
      contactComponent.contacts.push(contact);

    });
     Then(() => {
      expect(contactComponent.contacts.length).toBeGreaterThan(0);
    });
  });

});
